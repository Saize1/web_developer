const problem6=require("./1-arrays-jobs.cjs");
function averageofsalary(arrayobj){
   let sum= arrayobj.reduce((acc,curr)=>{
    if(acc[curr.location]){
        acc[curr.location]+=parseFloat(curr.salary.replace("$",""))
    }
    else{
        acc[curr.location]=parseFloat(curr.salary.replace("$",""))
    }
    return acc;
        
    },{})
    

    const country = arrayobj.reduce((acc,curr)=>{
        if(acc[curr.location]){
            acc[curr.location]++;
        }
        else{
            acc[curr.location] = 1;
        }
        return acc;
            
        },{})

    const result = arrayobj.reduce((acc, user) => {
        acc[user.location] = acc[user.location] / country[user.location]//toal sum of salaries/number of times country repeated
        return acc;
    }, sum)

    return result;
}
console.log(averageofsalary(problem6))